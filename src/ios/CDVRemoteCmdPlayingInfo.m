/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#import "CDVFile.h"
#import "CDVRemoteCmdPlayingInfo.h"

#define DOCUMENTS_SCHEME_PREFIX @"documents://"
#define CDVFILE_PREFIX @"cdvfile://"

@implementation CDVRemoteCmdPlayingInfo


- (NSURL*)urlForArtwork:(NSString*)resourcePath
{
    NSURL* resourceURL = nil;
    NSString* filePath = nil;
    
    // first try to find HTTP:// or Documents:// resources
    
    if ([resourcePath hasPrefix:@"http://"] || [resourcePath hasPrefix:@"https://"] || [resourcePath hasPrefix:@"file:///"]) {
        resourceURL = [NSURL URLWithString:resourcePath];
    }
    else if ([resourcePath hasPrefix:DOCUMENTS_SCHEME_PREFIX]) {
        NSString* docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        filePath = [resourcePath stringByReplacingOccurrencesOfString:DOCUMENTS_SCHEME_PREFIX withString:[NSString stringWithFormat:@"%@/", docsPath]];
        NSLog(@"Will use resource '%@' from the documents folder with path = %@", resourcePath, filePath);
    } else if ([resourcePath hasPrefix:CDVFILE_PREFIX]) {
        CDVFile *filePlugin = [self.commandDelegate getCommandInstance:@"File"];
        CDVFilesystemURL *url = [CDVFilesystemURL fileSystemURLWithString:resourcePath];
        filePath = [filePlugin filesystemPathForURL:url];
        if (filePath == nil) {
            resourceURL = [NSURL URLWithString:resourcePath];
        }
    } else {
        // attempt to find file path in www directory or LocalFileSystem.TEMPORARY directory
        filePath = [self.commandDelegate pathForResource:resourcePath];
        if (filePath == nil) {
            // see if this exists in the documents/temp directory from a previous recording
            NSString* testPath = [NSString stringWithFormat:@"%@/%@", [NSTemporaryDirectory()stringByStandardizingPath], resourcePath];
            if ([[NSFileManager defaultManager] fileExistsAtPath:testPath]) {
                // inefficient as existence will be checked again below but only way to determine if file exists from previous recording
                filePath = testPath;
                NSLog(@"Will attempt to use file resource from LocalFileSystem.TEMPORARY directory");
            } else {
                // attempt to use path provided
                filePath = resourcePath;
                NSLog(@"Will attempt to use file resource '%@'", filePath);
            }
        } else {
            NSLog(@"Found resource '%@' in the web folder.", filePath);
        }
    }
    // if the resourcePath resolved to a file path, check that file exists
    if (filePath != nil) {
        // create resourceURL
        resourceURL = [NSURL fileURLWithPath:filePath];
        // try to access file
        NSFileManager* fMgr = [NSFileManager defaultManager];
        if (![fMgr fileExistsAtPath:[resourceURL path]]) {
            NSLog(@"Unknown resource - path '%@'", [resourceURL path]);
            resourceURL = nil;
        }
    }
    
    return resourceURL;
}

- (void)create:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            NSDictionary* audioInfo = [command argumentAtIndex:0];
            if(!audioInfo)return;
            NSLog(@"CDVRemoteCmdPlayingInfo::create++");
            
            NSNumber *useRemoteCommandCenter=[audioInfo objectForKey:@"useRemoteCommandCenter"];
            if(!useRemoteCommandCenter || [useRemoteCommandCenter boolValue]){
                //we use remote command center
                MPRemoteCommandCenter *rcc = [MPRemoteCommandCenter sharedCommandCenter];
                
                //play
                MPRemoteCommand *playCommand = [rcc playCommand];
                [playCommand setEnabled:YES];
                [playCommand addTarget:self action:@selector(playEvent:)];
                
                //pause
                MPRemoteCommand *pauseCommand = [rcc pauseCommand];
                [pauseCommand setEnabled:YES];
                [pauseCommand addTarget:self action:@selector(pauseEvent:)];
                
                //togglePlayPause
                MPRemoteCommand *togglePPCommand = [rcc togglePlayPauseCommand];
                [togglePPCommand setEnabled:YES];
                [togglePPCommand addTarget:self action:@selector(playOrPauseEvent:)];
                
                //do we have skip forward?
                NSNumber *skipForwardValue=[audioInfo objectForKey:@"skipForwardValue"];
                if(skipForwardValue){
                    NSLog(@"CDVRemoteCmdPlayingInfo::create: we have skipForward value: %@",skipForwardValue);
                    MPSkipIntervalCommand *skipForwardIntervalCommand = [rcc skipForwardCommand];
                    skipForwardIntervalCommand.preferredIntervals = @[skipForwardValue];
                    [skipForwardIntervalCommand setEnabled:YES];
                    [skipForwardIntervalCommand addTarget:self action:@selector(skipForwardEvent:)];
                }
                
                //do we have skip backward?
                NSNumber *skipBackwardValue=[audioInfo objectForKey:@"skipBackwardValue"];
                if(skipBackwardValue){
                    NSLog(@"CDVRemoteCmdPlayingInfo::create: we have skipBackward value: %@",skipBackwardValue);
                    MPSkipIntervalCommand *skipBackwardIntervalCommand = [rcc skipBackwardCommand];
                    skipBackwardIntervalCommand.preferredIntervals = @[skipBackwardValue];
                    [skipBackwardIntervalCommand setEnabled:YES];
                    [skipBackwardIntervalCommand addTarget:self action:@selector(skipBackwardEvent:)];
                }
                
                //do we have nextTrack?
                NSNumber *nextValue=[audioInfo objectForKey:@"receiveNextTrackEvent"];
                if(nextValue){
                    NSLog(@"CDVRemoteCmdPlayingInfo::create: we use NextTrack (value: %@)",nextValue);
                    MPRemoteCommand *nextTrackCommand = [rcc nextTrackCommand];
                    [nextTrackCommand setEnabled:YES];
                    [nextTrackCommand addTarget:self action:@selector(nextTrackEvent:)];
                }
                
                //do we have prevTrack?
                NSNumber *prevValue=[audioInfo objectForKey:@"receivePrevTrackEvent"];
                if(prevValue){
                    NSLog(@"CDVRemoteCmdPlayingInfo::create: we use PrevTrack (value: %@)",prevValue);
                    MPRemoteCommand *prevTrackCommand = [rcc previousTrackCommand];
                    [prevTrackCommand setEnabled:YES];
                    [prevTrackCommand addTarget:self action:@selector(prevTrackEvent:)];
                }
            }

            NSNumber *useNowPlayingInfo=[audioInfo objectForKey:@"useNowPlayingInfo"];
            if(!useNowPlayingInfo || [useNowPlayingInfo boolValue]){
                [self updateInfo:command];
            }
			
            NSLog(@"CDVRemoteCmdPlayingInfo::create--");
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::create: %@", e);
        }
    }];
}

- (void)release:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            //remove all event targets
            MPRemoteCommandCenter *rcc = [MPRemoteCommandCenter sharedCommandCenter];
            [rcc.playCommand removeTarget:self];
            [rcc.pauseCommand removeTarget:self];
            [rcc.togglePlayPauseCommand removeTarget:self];
            
            [rcc.skipForwardCommand removeTarget:self];
            [rcc.skipBackwardCommand removeTarget:self];
            
            [rcc.nextTrackCommand removeTarget:self];
            [rcc.previousTrackCommand removeTarget:self];
            
            //clear the now playing info
            NSNumber *clearNowPlayingInfo=[command argumentAtIndex:0 withDefault:nil];
            if(clearNowPlayingInfo && [clearNowPlayingInfo boolValue]) {
                [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nil;
            }
        }
        @catch (NSException *e) {
            NSLog(@"Exception CDVRemoteCmdPlayingInfo::release: %@", e);
        }
    }];
}

- (void)updateInfo:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            NSMutableDictionary *updatedInfo=[NSMutableDictionary dictionary];
            NSDictionary* audioInfo = [command argumentAtIndex:0];
            if(!audioInfo)return;
            
            //do we have a title?
            NSString *infoTitle=[audioInfo objectForKey:@"title"];
            if(infoTitle) {
                [updatedInfo setObject:infoTitle forKey: MPMediaItemPropertyTitle];
            }
            
            //do we have an album title?
            NSString *infoAlbumTitle=[audioInfo objectForKey:@"albumTitle"];
            if(infoAlbumTitle) {
                [updatedInfo setObject:infoAlbumTitle forKey: MPMediaItemPropertyAlbumTitle];
            }
            
            //do we have a track number?
            NSNumber *infoAlbumTrackNumber=[audioInfo objectForKey:@"albumTrackNumber"];
            if(infoAlbumTrackNumber) {
                [updatedInfo setObject:infoAlbumTrackNumber forKey: MPMediaItemPropertyAlbumTrackNumber];
            }
            
            //do we have a track count?
            NSNumber *infoAlbumTrackCount=[audioInfo objectForKey:@"albumTrackCount"];
            if(infoAlbumTrackCount) {
                [updatedInfo setObject:infoAlbumTrackCount forKey: MPMediaItemPropertyAlbumTrackCount];
            }
            
            //do we have duration?
            NSNumber *infoPlaybackDuration=[audioInfo objectForKey:@"playbackDuration"];
            if(infoPlaybackDuration) {
                [updatedInfo setObject:infoPlaybackDuration forKey: MPMediaItemPropertyPlaybackDuration];
            }
            
            //do we have position?
            NSNumber *infoPlaybackPosition=[audioInfo objectForKey:@"playbackPosition"];
            if(infoPlaybackPosition) {
                [updatedInfo setObject:infoPlaybackPosition forKey: MPNowPlayingInfoPropertyElapsedPlaybackTime];
            }
            else {
                [updatedInfo removeObjectForKey: MPNowPlayingInfoPropertyElapsedPlaybackTime];
            }
            
            //do we have playback rate?
            NSNumber *infoPlaybackRate=[audioInfo objectForKey:@"playbackRate"];
            if(infoPlaybackRate) {
                [updatedInfo setObject:infoPlaybackRate forKey: MPNowPlayingInfoPropertyPlaybackRate];
            }
            
            //do we have artwork?
            NSString *infoArtwork=[audioInfo objectForKey:@"artwork"];
            if(infoArtwork){
                NSURL *url=[self urlForArtwork:infoArtwork];
                if(url){
                    MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage: [UIImage imageWithData: [NSData dataWithContentsOfURL:url]]];
                    if(artwork) {
                        [updatedInfo setObject: artwork forKey: MPMediaItemPropertyArtwork];
                    }
                }
            }
            
            //update the information
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = updatedInfo;
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::updateInfo: %@", e);
        }
    }];
}
- (void) playEvent:(MPRemoteCommandEvent*)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%d);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",1,0];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::playEvent: %@", e);
        }
    }];
}
- (void) pauseEvent:(MPRemoteCommandEvent*)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%d);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",2,0];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::pauseEvent: %@", e);
        }
    }];
}
- (void) playOrPauseEvent:(MPRemoteCommandEvent*)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%d);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",3,0];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::playOrPauseEvent: %@", e);
        }
    }];
}
- (void) skipForwardEvent:(MPSkipIntervalCommandEvent *)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSLog(@"XXXXXXXXX SkipForwardEvent: %@",event);
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%f);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",4,event.interval];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::skipForwardEvent: %@", e);
        }
    }];
}
- (void) skipBackwardEvent:(MPSkipIntervalCommandEvent *)event {
    [self.commandDelegate runInBackground:^ {
        @try {
            NSLog(@"XXXXXXXXX SkipBackwardEvent: %@",event);
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%f);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",5,event.interval];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in CDVRemoteCmdPlayingInfo::skipBackwardEvent: %@", e);
        }
    }];
}
- (void) nextTrackEvent:(MPRemoteCommandEvent *)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSLog(@"XXXXXXXXX NextTrackEvent: %@",event);
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%f);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",6,0.];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in updateInfo: %@", e);
        }
    }];
}
- (void) prevTrackEvent:(MPRemoteCommandEvent *)event {
    [self.commandDelegate runInBackground:^{
        @try {
            NSLog(@"XXXXXXXXX NextTrackEvent: %@",event);
            NSString* jsString = [NSString stringWithFormat:@"%@(%d,%f);",
                        @"cordova.require('cordova-plugin-remotecmd-playinginfo.RemoteCmdPlayingInfo').onEvent",7,0.];
            [self.commandDelegate evalJs:jsString];
        }
        @catch (NSException *e) {
            NSLog(@"Exception in updateInfo: %@", e);
        }
    }];
}

@end